function RedCardsIssuedPerTeam(matches, players, year) {
  //make a set of the MatchID that was played in 2014. (used spread operator then filter according the match year 2014 and them map only MatchID of all filtered matches in a set)
  const setList = new Set([
    ...matches
      .filter((matchYear) => matchYear["Year"] == year)
      .map((item) => item.MatchID),
  ]);

  /* make a list of player, 
  first we filter the players data according their MatchID that is present in the setList ,
  then using chaning use another filter that give the data of the player that have include "R" in there Event. 
  */

  const playerWithRed = players
    .filter((item) => setList.has(item.MatchID))
    .filter((item) => item.Event.includes("R"));

  const teamRedCard = playerWithRed.reduce((output, team) => {
    if (output[team["Team Initials"]]) {
      output[team["Team Initials"]]++;
    } else {
      output[team["Team Initials"]] = 1;
    }
    return output;
  }, {});

  //make an object of all the team name with the TeamInitial by matching "Away Team Initials" and "Home Team Initials" from matchesAll list

  const allTeam = matches.reduce((output, match) => {
    if (!output[match["Away Team Initials"]]) {
      match["Away Team Initials"] = match["Away Team Name"];
    }
    if (!output[match["Home Team Initials"]]) {
      output[match["Home Team Initials"]] = match["Home Team Name"];
    }
    return output;
  }, {});

  //finally make a new object of teamName and no of red card pair using the allTeam and freqObj data.
  const result = {};
  for (let key in teamRedCard) {
    result[allTeam[key]] = teamRedCard[key];
  }
  return result;
}

module.exports = RedCardsIssuedPerTeam;
