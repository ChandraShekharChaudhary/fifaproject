function top10PlayerHighestProb(players) {

  const playersGoal = players.reduce((output, player) => {
    let goal = player["Event"].split("G").length - 1;
    if (output[player["Player Name"]]) {
      output[player["Player Name"]] = output[player["Player Name"]] + goal;
    } else {
      output[player["Player Name"]] = goal;
    }
    return output;
  }, []);


  const noOfGames = players.reduce((output, player) => {
    if (output[player["Player Name"]]) {
      output[player["Player Name"]] = output[player["Player Name"]] + 1;
    } else {
      output[player["Player Name"]] = 1;
    }
    return output;
  }, {});


  let finalList = {};
  for (let name in noOfGames) {
    let prob = playersGoal[name] / noOfGames[name];
    finalList[name] = prob;
  }


  const arrList = Object.entries(finalList);
  arrList.sort((a, b) => b[1] - a[1]);
  const top10Playar = arrList.splice(0, 10);
  const result = Object.fromEntries(top10Playar);
  return result;
}

module.exports = top10PlayerHighestProb;
