function numberOfMatchesPlayedPerCity(matches) {
const cities = matches.reduce((result, match)=>{
    if (result[match["City"]]) {
      result[match["City"]]++;
    } else {
      result[match["City"]] = 1;
    }
    return result;
  }, {});

  return cities;
}

module.exports = numberOfMatchesPlayedPerCity;
