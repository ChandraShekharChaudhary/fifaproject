function numberOfMatchesWonPerTeam(worldCups){
    const list=worldCups.reduce((result, worldCup)=>{
        if(result[worldCup['Winner']]){
            result[worldCup['Winner']]++;
        }
        else{
            result[worldCup['Winner']]=1;
        }
        return result;
    },{})
    
    return list;
}

module.exports=numberOfMatchesWonPerTeam;