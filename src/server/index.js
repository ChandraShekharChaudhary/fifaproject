const fs = require("fs"); //fs-file system
const csv = require("csvtojson");
let numberOfMatchesPlayedPerCity = require("./numberOfMatchesPlayedPerCity.cjs");
let numberOfMatchesWonPerTeam = require("./numberOfMatchesWonPerTeam.cjs");
let top10PlayerHighestProb = require("./top10PlayerProbabilityToHighestGoal.cjs");
let numberOfRedCardIssuedPerTeamIn2014Worldcup = require("./numberOfRedCardIssuedPerTeamIn2014Worldcup.cjs");

1.
csv()
  .fromFile("../data/WorldCupMatches.csv")
  .then((matches) => {
    const jsonString = JSON.stringify(numberOfMatchesPlayedPerCity(matches));
    fs.writeFile(
      "../public/output/numberOfMatchesPlayedPerCity.json",
      jsonString,
      (err) => {
        if (err) {
          throw err;
        } else {
          console.log("file created");
        }
      }
    );
  });

//2.
csv()
  .fromFile("../data/WorldCups.csv")
  .then((worldCups) => {
    const output2 = JSON.stringify(numberOfMatchesWonPerTeam(worldCups));
    fs.writeFile(
      "../public/output/numberOfMatchesWonPerTeam.json",
      output2,
      (err) => {
        if (err) {
          throw err;
        } else {
          console.log("file created");
        }
      }
    );
  });

  //3.
csv()
  .fromFile("../data/WorldCupPlayers.csv")
  .then((players) => {
    const output3 = JSON.stringify(top10PlayerHighestProb(players));
    fs.writeFile(
      "../public/output/top10PlayerProbabilityToHighestGoal.json",
      output3,
      (err) => {
        if (err) {
          throw err;
        } else {
          console.log("file created");
        }
      }
    );
  });

//4.
let matchesPath = "../data/WorldCupMatches.csv";
let playersPath = "../data/WorldCupPlayers.csv";

csv()
  .fromFile(playersPath)
  .then((player) => {
    csv()
      .fromFile(matchesPath)
      .then((matches) => {
        const output5 = JSON.stringify(
          numberOfRedCardIssuedPerTeamIn2014Worldcup(matches, player, 2014)
        );
        fs.writeFile(
          "../public/output/numberOfRedCardIssuedPerTeamIn2014Worldcup.json",
          output5,
          (err) => {
            if (err) {
              throw err;
            } else {
              console.log("file created");
            }
          }
        );
      });
  });
